## Установка
Установите [Yarn](https://yarnpkg.com/en/docs/install).
* введите команду: `yarn dev` (режим разработки);
* чтобы «собрать» проект, введите команду `yarn build`.
## Предустановленные пакеты
Удалите ненужные
* [fancybox](http://fancyapps.com/fancybox/3/)
* [inputmask](https://github.com/RobinHerbots/Inputmask)
* [jquery](https://api.jquery.com/)
* [lazysizes](https://github.com/aFarkas/lazysizes)
* [swiper](https://swiperjs.com/)

### Сборка проекта в режиме разработки
`yarn run dev`

## Окончательная сборка
`yarn run build`

### Структура каталогов
```
frontend-pack
├── dist Собранный проект
│   └── assets Исходники из src/static
│       ├── js Скомпилированные js
│       └── css Скомпилированные css
└── src Исходные файлы
    ├── icons Исходники svg-спрайта
    └── js js-файлы
        └── libs Сторонние библиотеки, которые не получается подключить динамически
    ├── locales Языковые файлы
    └── scss
        ├── components Самодостаточные компоненты
        ├── fonts Шрифты
        ├── generated Сгенерированный спрайты
        ├── img Изображения, используемые в стилях
        ├── templates Шаблоны спрайтов
        └── vendor Сторонние библиотеки
    ├── sprites Исходники png-спрайта (используйте @2 в названии для двойной плотности)
    └── static Статические файлы
        ├── ajax То, что будем загружать через аякс
        └── img Изображения
    └── views pug-шаблоны
        └── _includes Подключаемые...
            └── blocks ...блоки
                └── popups ...попапы
            └── mixins ...михины
        └── popups попапы
```