import $ from 'jquery';
import Inputmask from 'inputmask';

let forms = {

  init: function() {
    forms.app = this;

    this.document.ready(() => {
      forms.initMask();
    });
  },

  initMask() {
    let selector = document.querySelectorAll('.mask-validate');
    Inputmask({
      mask: '+7 (999) 999 99 99',
      showMaskOnHover: false,
    }).mask(selector);

  },

};

export default forms;