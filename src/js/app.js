import 'jquery.global.js';
import fancybox from '@fancyapps/fancybox';
import page from 'page';
import forms from 'forms';
import slick from 'slick-carousel';
import {
  disableBodyScroll,
  enableBodyScroll,
  clearAllBodyScrollLocks
} from 'body-scroll-lock';

import Swiper from 'swiper/bundle';

import mainPage from 'index.page';


let app = {


  scrollToOffset: 200, // оффсет при скролле до элемента
  scrollToSpeed: 500, // скорость скролла

  init: function () {
    // read config
    if (typeof appConfig === 'object') {
      Object.keys(appConfig).forEach(key => {
        if (Object.prototype.hasOwnProperty.call(app, key)) {
          app[key] = appConfig[key];
        }
      });
    }

    app.currentID = 0;

    // Init page
    this.page = page;
    this.page.init.call(this);

    this.forms = forms;
    this.forms.init.call(this);

    // Init page

    this.mainPage = mainPage;
    this.mainPage.init.call(this);

    //window.jQuery = $;
    window.app = app;

    app.document.ready(() => {


    });



    app.window.on('load', () => {


      let $labelChec = document.querySelectorAll('.input-checkbox');
      let $stat = document.querySelector('#stat')
      let $arrow = document.querySelector('.arrow')
      const $input = document.querySelectorAll('.input');
      const $label = document.querySelectorAll('.label-item')
      $labelChec.forEach((item) => {
        item.addEventListener('change', () => {
          let $checkList = document.querySelectorAll('.input-checkbox:checked');
          let $checkNum = $checkList.length



          switch ($checkNum) {
            case 0:
              numAnimate(0, -1);
              numArrow(-35);
              break;
            case 1:
              numAnimate(100, 0);
              numArrow(-20);
              break;

            case 2:
              numAnimate(200, 100);
              numArrow(-10);
              break;

            case 3:
              numAnimate(300, 200);
              numArrow(0);
              break;

            case 4:
              numAnimate(400, 300);
              numArrow(10);
              break;

            case 5:
              numAnimate(500, 400);
              numArrow(20);
              break;

            case 6:
              numAnimate(600, 500);
              numArrow(50);
              break;

            case 7:
              numAnimate(700, 600);
              numArrow(55);
              break;

            case 8:
              numAnimate(800, 700);
              numArrow(65);
              break;

            case 9:
              numAnimate(900, 800);
              numArrow(80);
              break;

            case 10:
              numAnimate(1000, 900);
              numArrow(100);
              break;

            case 11:
              numAnimate(1100, 1000);
              numArrow(110);
              break;

            case 12:
              numAnimate(1200, 1100);
              numArrow(150);
              break;
          }




        })

        function numAnimate(item, count) {
          setInterval(() => {
            count++;

            if (count <= item) {
              $stat.innerHTML = count
            }
          })
        }

        function numArrow(item) {
          $arrow.style.setProperty('--transform-rotate', item + 'deg')
        }

        numArrow(-35);



      });

      $input.forEach(item => {
        item.addEventListener('change', () => {
          if (item.value !== '') {
            item.closest('.input-item').classList.add('active')
          }
        })
      })
    });

    // this.document.on(app.resizeEventName, () => {
    // });

  },

};
app.init();